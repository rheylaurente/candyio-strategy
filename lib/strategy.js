var util                =   require('util');
var passport            =   require('passport');
var request             =   require('request');
var OAuth2Strategy      =   require('passport-oauth2');

function Strategy(options, verify) {
  options = options || {};
  options.authorizationURL = options.authorizationURL || 'http://candyio.local/auth/login';
  options.tokenURL = options.tokenURL || 'http://candyio.local/auth/validate';
  options.scopeSeparator = options.scopeSeparator || ',';

  OAuth2Strategy.call(this, options, verify);
  this.name = 'candyio';
  this._clientSecret = options.clientSecret;
  this._enableProof = options.enableProof;
  this._profileURL = options.profileURL || 'http://candyio.local/api/users/current';
  this._profileFields = options.profileFields || null;
}

util.inherits(Strategy, OAuth2Strategy);

Strategy.prototype.authenticate = function(req, options) {
  OAuth2Strategy.prototype.authenticate.call(this, req, options);
};

Strategy.prototype.userProfile = function(token, done) {
    request.get('http://candyio.local/api/users/current', {
        auth    :   {
          bearer    : token
        }
    }, function(err, res){
        var profile         =   null;
        if(!err && res && res.body){
            var response    =   JSON.parse(res.body);
            if(response && response.data){
                profile     =   response.data;
            }
        }else{
            err =   "Can't access profile";
        }
        done(err, profile);
    });
};

module.exports = Strategy;